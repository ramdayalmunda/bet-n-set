const express = require('express');
const router = express.Router();

const betController = require("../controller/bet.js");
const { verifyUser } = require('../controller/authentication.js');

router.post("/place", verifyUser,  betController.placeBet)

module.exports = router