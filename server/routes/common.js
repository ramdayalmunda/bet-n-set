const express = require('express');
const commonController = require('../controller/common/user.js');
const router = express.Router();

router.post( "/login", commonController.login );
router.post( "/register", commonController.register );

module.exports = router