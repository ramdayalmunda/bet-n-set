const CONFIG = require('./config/config.js')
const express = require('express');
const app = express()
const bodyParser = require('body-parser');
const cors = require('cors');
const mongoose = require('mongoose');
app.use( cors() )
app.use( bodyParser.json() );

app.use('/common',require('./routes/common.js'))
app.use('/bet', require('./routes/bet.js'))

app.listen( CONFIG.SERVER_PORT, ()=>{
    console.log(`server on http://127.0.0.1:${CONFIG.SERVER_PORT} `)
    mongoose.connect( CONFIG.MONGO_CONFIG.url )
        .then( ()=>{ console.log('--mongoDb connection established') } )
        .catch( (err)=>{ console.log('--failed to connect with mongoDb') } )
} );
