const path = require("path")
const CLIENT_PORT = 4001;
const SERVER_PORT = 9001;
const MONGO_CONFIG = {
    url: "mongodb://127.0.0.1:27017/betnset",
}
const PASSWORD = {
    hashSalt: 10,
    privateKey: "adalov"
}

const PATH= {
    basePath: path.join( __dirname ),
    logger: path.join( __dirname, '../logs/' ),
}

module.exports = {
    CLIENT_PORT,
    SERVER_PORT,
    MONGO_CONFIG,
    PASSWORD,
    PATH,
}