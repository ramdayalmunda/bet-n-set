const fs = require('fs');
const path = require('path');
const { PATH } = require('../config/config');


const logger = function (title, error) {
    let fileName = `${new Date().toISOString().slice(0, 10)}.txt`
    let filePath = PATH.logger + fileName;


    let errorString = `---------------------------
${new Date()}:
${title}
${error}

`;

    fs.appendFile(filePath, errorString, (err) => {
        if (err) console.log(err);
    });
}

module.exports = logger;