const MESSAGES = {
    "SUCCESS":"200: Everything is OK",
    "INTERNAL_SERVER_ERROR": "500: Internal Server Error",
    "AUTHENTICATION_FAILED": "511 Authentication Failed",

    "LOGGED_IN": "You have logged in successfully",
    "INVALID_CREDENTIALS":"Invalid Credentials",
    "LOGGED_OUT": "You have logged out successfully",
    "ACCOUNT_REGISTERED": "Your account has been registered.",
    "USER_ALREADY_PRESENT": "User with this credentials is already present.",

    "BET_PLACED": "Bet has been placed successfully.",
}
module.exports = MESSAGES