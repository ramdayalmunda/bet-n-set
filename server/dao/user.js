const model = require("../model/index.js")

module.exports.addUser = async (userData)=>{
    let userObj = await model.User(userData);
    await userObj.save();
    return userObj
}

module.exports.checkIfUserPresent = async (username, email) =>{
    return await model.User.findOne({
        $or: [
            { username: username },
            { email: email }
        ]
    }, { _id: 1 })
}
module.exports.findUserByNameOrEmail = async (userOrEmail)=>{
    return await model.User.findOne({
        $or: [
            { username: userOrEmail },
            { email: userOrEmail }
        ]
    },{ _id: 1, username: 1, email: 1, password: 1 })
}

module.exports.getUserByOid = async (userOid)=>{
    return await model.User.findById( userOid )
}