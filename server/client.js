const CONFIG = require('./config/config.js')
const express = require('express');
const app = express()
const path = require('path')
const axios = require('axios');
const bodyParser = require('body-parser')

app.use( bodyParser.json() )

app.use( express.static(path.join( __dirname, '../client/' )) )

let serverURL = `http://127.0.0.1:${CONFIG.SERVER_PORT}`;
const api = axios.create({ baseURL: serverURL })

app.use('/api', async (req, res)=>{
    let method = req.method.toLowerCase()
    api.headers = {
        'Content-Type': 'application/json',
    }
    let params = {};
    if (Object.entries(req.query).length){ params ={ params: req.query } };
    let body= req.body?req.body:{}
    let response;
    try{
        response = await api[method](req.path, {
            ...params,
            ...body
        })
        res.json(response.data)
    }catch(err){
        console.log(err)
        console.log('ERROR while sending request')
        res.json({message: "could not redirect"})
    }

})

app.get("/*", (req, res)=>{ res.sendFile( path.join( __dirname, '../client/index.html' ) ) })

app.listen( CONFIG.CLIENT_PORT, ()=>{ console.log(` server on http://127.0.0.1:${CONFIG.CLIENT_PORT} `) } );
