const mongoose = require('mongoose');

const schemaObject = {
    username: { type: String, required: true },
    password: String,
    fullName: String,
    dob: Date,
    email: String,
}

module.exports = {
    schema: new mongoose.Schema(schemaObject),
    modelName: "User",
}