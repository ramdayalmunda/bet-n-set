const mongoose = require('mongoose');
const userModel = require('./user.js')
const model = {
    [ userModel.modelName ]: mongoose.model(userModel.modelName, userModel.schema)
};


module.exports = model;