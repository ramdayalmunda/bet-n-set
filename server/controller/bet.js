const logger = require("../helper/logger.js");
const messages = require("../helper/messages.js")

module.exports.placeBet = async function( req, res ){
    try{
        res.json( { success: true, message: messages.BET_PLACED } )
    }catch(error){
        console.log(error)
        res.json( { success: false, message: messages.INTERNAL_SERVER_ERROR } );
        logger( "ERROR while placing bet", error )
    }
}

