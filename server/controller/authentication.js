const jwt = require("jsonwebtoken");

const { AUTHENTICATION_FAILED } = require("../helper/messages.js");
const { PASSWORD } = require("../config/config.js");
const { getUserByOid } = require("../dao/user.js")
const logger = require("../helper/logger.js")

module.exports.verifyUser = async (req, res, next) => {
    try {
        const { token } = req.headers;
        let tokenObj = jwt.verify( token, PASSWORD.privateKey )
        req.userData = await getUserByOid(tokenObj.userOid)
        next()

    } catch (error) {
        res.json({ success: false, message: AUTHENTICATION_FAILED })
        logger( "ERROR in authenticating middleware", error )
    }

}