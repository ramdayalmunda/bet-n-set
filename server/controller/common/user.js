const userDao = require("../../dao/user");
const MESSAGES = require("../../helper/messages")
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

const { PASSWORD } = require("../../config/config");
const logger = require("../../helper/logger");

module.exports.login = async (req, res)=>{
    try{
        const { userOrEmail, password } = req.body;

        let passHash = bcrypt.hashSync( password, PASSWORD.hashSalt )
        let userFound = await userDao.findUserByNameOrEmail(userOrEmail)
        if ( userFound && bcrypt.compareSync( password, passHash ) ){
            let token = jwt.sign({
                username: userFound.username,
                userOid: userFound._id,
                email: userFound.email,
            }, PASSWORD.privateKey)
            res.json({success:true, message: MESSAGES.LOGGED_IN, userData: {
                username: userFound.username,
                email: userFound.email,
                token: token,
            }})
        }else{
            res.json({success:false, message: MESSAGES.INVALID_CREDENTIALS})
        }

    }catch(err){
        res.json({success: false, message: MESSAGES.INTERNAL_SERVER_ERROR});
        logger( "ERROR in login function", err )
    }
}
module.exports.register = async function(req, res){
    try{
        const userData = {
            username: req.body.username,
            email: req.body.email,
            password: req.body.password,
        }
        let userPresent = await userDao.checkIfUserPresent( userData.username, userData.email )
        if (userPresent){
            res.json( {success: false, message: MESSAGES.USER_ALREADY_PRESENT} )
            return
        }
        userData.password = bcrypt.hashSync( userData.password, PASSWORD.hashSalt );
        let result = await userDao.addUser(userData);
        
        res.json({success:true, message: MESSAGES.ACCOUNT_REGISTERED, userData: result})

    }catch(err){
        res.json({success: false, message: MESSAGES.INTERNAL_SERVER_ERROR})
        logger( "ERROR in register function", err )
    }
}

