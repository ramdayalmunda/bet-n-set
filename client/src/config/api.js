import { getLocalStore } from "./localstore.js";

const origin = window.origin;
const serverPathName = "/api"
const api = axios.create({
    baseURL: origin+serverPathName,
})


export {
    api,
}