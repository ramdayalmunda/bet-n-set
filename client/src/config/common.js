import {getLocalStore} from "./localstore.js";

function getAuthHeaders(){
    return {
        headers: {
            "token": getLocalStore("userData")?.token
        }
    }
}


export {
    getAuthHeaders,
}