const classColor = {
    "success": "#198754",
    "warning": "#fd7e14",
    "danger": "#dc3545",
}
function showToast(text, className){
    Toastify( {
        text: text,
        position: 'left',
        backgroundColor: classColor[className],
        close: true,
    } ).showToast()
}
export {
    showToast,
}