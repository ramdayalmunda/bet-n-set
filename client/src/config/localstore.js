function setLocalData(data, storeName){
    let storeData = localStorage.getItem(storeName)
    let newStoreData = data
    if(storeData){
        storeData = JSON.parse(storeData)
        newStoreData = {
            ...newStoreData,
            ...storeData,
        }
    }
    localStorage.setItem(storeName, JSON.stringify(newStoreData))
    return newStoreData
}

function getLocalStore(storeName){
    let storeData = localStorage.getItem(storeName)
    if(storeData) storeData = JSON.parse(storeData)
    return storeData;
}

function clearLocalStore(storeName){
    localStorage.removeItem(storeName)
}

export {
    setLocalData,
    getLocalStore,
    clearLocalStore,
}