import { isLoggedIn } from "../components/authentication/authentication.js";

const routes = [
    { path: '/', name: "Login", component: ()=>import("../components/authentication/login/login.js"), authenticate: false },
    { path: '/register', name: "Register", component: ()=>import("../components/authentication/register/register.js"), authenticate: false },
    { path: '/profile', name: "Profile", component: ()=>import("../components/profile/profile.js"), authenticate: true },
    { path: '/bet', name: "Bet", component: ()=>import("../components/bet/bet.js"), authenticate: true },
    { path: '/event', name: "Event", component: ()=>import("../components/event/event.js"), authenticate: true },
]
const routesData = {}
routes.map( item => {
    routesData[item.name]= {
        path: item.path,
        name: item.name,
        authenticate: item.authenticate
    }
} )

const router = VueRouter.createRouter({
    routes,
    history: VueRouter.createWebHistory(),
})

router.beforeEach(async(to, from, next)=>{

    if ( to.name!='Login' && routesData[to.name].authenticate){
        if ( isLoggedIn() ) next()
        else next({name: "Login"})
    }
    else{
        if ( isLoggedIn() ){
            next({name:"Bet"})
        }
        else next()
    }
    return
})

export default router;