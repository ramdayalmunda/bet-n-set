import headComponent from "./components/header/header.js"
import { isLoggedIn } from "./components/authentication/authentication.js"

const { ref, onBeforeMount } = Vue;
const app = {
    components: {
        headComponent,
    },
    setup(){
        let appData = ref({
            loggedIn: false,
        })
        onBeforeMount( function (){
            if (isLoggedIn()) appData.value.loggedIn = true
            return
        } )
        return {
            appData,
        }
    },
    template: /*html*/`
        <headComponent :appData="appData" />
        <div class="container-fluid p-0 m-0">    
            <RouterView :appData="appData" />
        </div>
    `
}

export default app