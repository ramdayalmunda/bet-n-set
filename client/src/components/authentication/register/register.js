import { api } from "../../../config/api.js"
import { showToast } from "../../../config/toast.js";
import registerTemplate from "./register-template.js";
const { ref } = Vue;
const { useRouter } = VueRouter;
const registerComponent = {
    template: registerTemplate,
    setup() {
        const router = useRouter()
        const userData = ref({
            username: "",
            email: "",
            password: "",
            confirmPassword: "",
        })
        async function register() {
            await api.post('/common/register', userData.value)
                .then((response) => {
                    if (response.data.success){
                        showToast(response.data.message, "success")
                        router.push({name: "Login"})
                    } else {
                        showToast(response.data.message, "warning")
                    }
                }).catch((err) => {
                    console.log(err)
                    showToast(response.data.message, "danger")
                })
        }
        return {
            userData,

            register,
        }
    }
}

export default registerComponent