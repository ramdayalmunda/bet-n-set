export default /*html*/`
<div class="container-fluid mt-2">
    <form @submit.prevent="register">
        <div class="d-flex justify-content-center ">
            <div class="card col-12 col-sm-12 col-sm-8 col-md-6 col-lg-5 col-xl-4 p-3">
                
                <h2 class="justify-content-center">Register</h2>
            
                <input v-model="userData.email" class="form-control mt-2" type="email" placeholder="email" required />
                <input v-model="userData.username" class="form-control mt-2" type="text" placeholder="username" required />
                <input v-model="userData.password" class="form-control mt-2" type="password" placeholder="password" required />
                <input v-model="userData.confirmPassword" class="form-control mt-2" type="password" placeholder="confirm password" required />
                <button class="btn btn-primary mt-2">Submit</button>
            </div>
        </div>
    </form>
</div>
`