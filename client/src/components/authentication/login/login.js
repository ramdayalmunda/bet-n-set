import { clearLocalStore, setLocalData } from "../../../config/localstore.js";
import loginTemplate from "./login-template.js"
import { showToast } from "../../../config/toast.js"
import { api } from "../../../config/api.js";

const { useRouter } = VueRouter;
const { ref, onMounted } = Vue;
const loginComponent = {
    template: loginTemplate,
    props: {
        appData: { Object },
    },
    setup(props) {
        let userData = ref({
            userOrEmail: "",
            password: "",
        })
        const router = useRouter();
        async function login() {

            try {
                let response = await api.post('/common/login', userData.value)
                if (response.data.success) {
                    setLocalData({
                        username: response.data.userData.username,
                        email: response.data.userData.email,
                        token: response.data.userData.token
                    }, 'userData')
                    props.appData.loggedIn = true;
                    router.push({ name: "Bet" })
                }
                showToast(response.data.message, (response.data.success?'success':'warning'))
            } catch (err) {
                console.log(err)
                showToast("Internal Server Error", 'danger')
            }
        }

        onMounted(async function () {

        })
        return {
            userData,
            login,
        }
    }
}

export default loginComponent