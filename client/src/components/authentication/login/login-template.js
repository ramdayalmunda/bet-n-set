export default /*html*/`

<div class="container-fluid">
    <form @submit.prevent="login">
        <div class="d-flex justify-content-center ">
            <div class="card col-12 col-sm-12 col-sm-8 col-md-6 col-lg-5 col-xl-4 p-3">
                
                <h2 class="justify-content-center">Login</h2>
            
                <input v-model="userData.userOrEmail" class="form-control mt-2" type="text" placeholder="username or email" />
                <input v-model="userData.password" class="form-control mt-2" type="password" placeholder="password" />
                <button class="btn btn-primary mt-2">Login</button>
            </div>
        </div>
    </form>
</div>
`