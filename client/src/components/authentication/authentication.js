import { getLocalStore } from "../../config/localstore.js";

function isLoggedIn(){
    let userData = getLocalStore('userData')
    return (userData?.token)?true:false
}

export {
    isLoggedIn,
}