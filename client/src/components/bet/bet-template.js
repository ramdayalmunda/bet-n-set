export default /*html*/`
<div class="card p-2 m-2">
    <div class="card-body">
        <h5 class="card-title mb-3">Matches</h5>
        <div class="row row-cols-1 row-cols-md-2 row-cols-lg-3 g-4">
            <template v-for="(match, m) in matchlist">
                <div class="col">
                    <div class="card">
                        <img v-if="match.logo" :src="match.logo" class="card-img-top" alt="...">
                        <div class="card-body">
                            <h5 class="card-title">{{match.title}}</h5>
                            <p class="card-text">
                                <template class="" v-for="(option, o) in match.options">
                                    <span class="badge bg-secondary mx-2">{{ o==0?'':'Vs' }}</span>
                                    <span>{{option.name}}</span>
                                </template>
                            </p>
                            <p class="card-text row">
                                <span class="ms-2">bets placed : {{match.betsPlaced}}</span>
                                <span class="ms-2">pot : {{match.pot}}</span>
                                <span class="ms-2">stakes :
                                    <span class="badge bg-warning ms-1"
                                        v-for="(stake, s) in match.stakes">{{stake.price}}</span>
                                </span>
                            </p>
                            <a class="btn btn-primary" data-bs-toggle="modal" href="#place-bet-modal"
                                @click="setBetPopup(match)">Place</a>
                        </div>
                    </div>
                </div>

            </template>
        </div>
    </div>
</div>

<!-- Modal -->
<div v-show="popupData.showPopup" class="modal fade" id="place-bet-modal" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Bet on: {{popupData?.matchData?.title}}</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body p-0">
                <ul class="list-group list-group-flush" v-if="popupData?.matchData">
                    <li class="list-group-item fw-bold text-center">
                        <template v-for="(option, o) in popupData?.matchData?.options">
                            <span>{{o==0?'':' Vs '}}{{option.name}}</span>
                        </template>
                    </li>
                    <template v-for="(option, o) in popupData?.matchData?.options">
                        <li class="list-group-item">
                            <div class="container m-0 p-0">
                                <div class="form-switch">
                                    <input :id="'popup-option-'+o" :value="option"
                                        v-model="popupData.matchData.optionSelected" class="form-check-input"
                                        name="radio-bet" type="radio">
                                    <label class="cursor-pointer ms-2 form-check-label"
                                        :for="'popup-option-'+o">{{option.name}}</label>
                                </div>
                            </div>
                        </li>
                    </template>
                    <li class="list-group-item">

                        <div class="container mb-2">
                            <div class="row pt-2">
                                <div class="col-4 text-center p-0 m-0">Stakes</div>
                                <div class="col-8 text-center p-0 m-0">Amount</div>
                            </div>
                            <template v-for="(stake, s) in popupData?.matchData?.stakes">

                                <div class="row pt-2">
                                    <div class="col-4">
                                        <label class=" form-control text-center"
                                            :title="'stake-slot-benefit '+stake.x">&#8377;{{stake.price}}</label>
                                    </div>
                                    <div class="col-8">
                                        <div class="input-group">
                                            <button @click="setBets(false, stake)"
                                                class="input-group-text">&minus;</button>
                                            <label class="form-control text-center">
                                                {{stake?.betsPlaced?stake.betsPlaced:0}}
                                            </label>
                                            <label class="form-control text-center">
                                                &#8377;{{stake?.betAmount?stake.betAmount:0}}
                                            </label>
                                            <button @click="setBets(true, stake)"
                                                class="input-group-text">&plus;</button>
                                        </div>
                                    </div>
                                </div>
                            </template>
                        </div>
                    </li>
                    <li class="list-group-item">

                        <div class="container mb-2">
                            <div class="row pt-2">
                                <div class="col-6 text-start p-0 m-0">
                                    Total: &#8377;{{popupData?.matchData?.betAmount?popupData.matchData.betAmount:0}}
                                </div>
                                <div class="col-6 text-end p-0 m-0">
                                    You have: &#8377;2033
                                </div>
                            </div>
                        </div>
                    </li>
                </ul>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                <button type="button" data-bs-toggle="modal" data-bs-target="#place-bet-confirm-modal"
                    class="btn btn-primary"
                    :class="(popupData?.matchData?.betAmount && popupData?.matchData?.optionSelected)?'':'disabled'">
                    Place Bet
                </button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="place-bet-confirm-modal">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Confirmation</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <p>Are you sure you would like to place the following bet?</p>
                <p>&#8377;{{popupData?.matchData?.betAmount}} on {{popupData?.matchData?.optionSelected?.name}} on event {{popupData?.matchData?.title}}</p>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-toggle="modal" data-bs-dismiss="modal"
                    data-bs-target="#place-bet-modal">No</button>
                <button @click="placeBet" type="button" class="btn btn-primary" data-bs-dismiss="modal">Yes</button>
            </div>

        </div>
    </div>
</div>
`