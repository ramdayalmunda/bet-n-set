const PROXY = {
    matchList: [
        {
            _id: "1",
            betsPlaced: 120,
            stakes: [
                { id: 0, price: 20, x: 0.5 },
                { id: 1, price: 40, x: 1 },
                { id: 2, price: 80, x: 1.5 },
            ],
            pot: 4460, // amount of money collected
            title: "WWE heavy weight championship",
            summary: "",
            logo: "",
            options: [
                {
                    _id: "0",
                    name: "The Undertaker",
                },
                {
                    _id: "1",
                    name: "Brock Lesner"
                }
            ]
        },
        {
            _id: "3",
            betsPlaced: 53,
            stakes: [
                { id: 0, price: 70, x: 0.5 },
                { id: 1, price: 140, x: 1 },
                { id: 2, price: 200, x: 1.5 },
            ],
            pot: 3630, // amount of money collected
            title: "MSL quater finals 1st leg",
            logo: "",
            options: [
                {
                    _id: "0",
                    name: "Liverpool",
                },
                {
                    _id: "1",
                    name: "Manchester United"
                }
            ]
        },
        {
            _id: "2",
            betsPlaced: 4760,
            stakes: [
                { id: 0, price: 50, x: 0.5 },
                { id: 1, price: 100, x: 1 },
                { id: 2, price: 150, x: 2 },
            ],
            pot: 160, // amount of money collected
            title: "MSL quater finals 2nd leg",
            logo: "",
            options: [
                {
                    _id: "0",
                    name: "Arsenal",
                },
                {
                    _id: "1",
                    name: "Totenham Hotspurs"
                }
            ]
        },
        {
            _id: "4",
            betsPlaced: 8652,
            stakes: [
                { id: 0, price: 30, x: 0.5 },
                { id: 1, price: 60, x: 1 },
                { id: 2, price: 90, x: 1.5 },
            ],
            pot: 23, // amount of money collected
            title: "WWE Divas triple-threat",
            logo: "",
            options: [
                {
                    _id: "0",
                    name: "Josie Maran",
                },
                {
                    _id: "1",
                    name: "Laura Darts"
                },
                {
                    _id: "2",
                    name: "Megan Fox"
                }
            ]
        },
    ]
}

export default PROXY