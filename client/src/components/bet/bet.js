import { api } from "../../config/api.js";
import { getAuthHeaders } from "../../config/common.js";
import { showToast } from "../../config/toast.js";
import betTemplate from "./bet-template.js"
import PROXY from "./proxy.js";
const { ref, onMounted } = Vue;
const betComponent = {
    template: betTemplate,
    setup() {
        function basicPopupData() {
            return {
                showPopup: false,
            }
        }
        let matchlist = ref([])
        let popupData = ref(new basicPopupData())


        function setBetPopup(matchData) {
            popupData.value.matchData = JSON.parse(JSON.stringify(matchData))
        }
        function resetBetPopup() {
            delete popupData.value.matchData
        }

        function tempProxyMatch() {
            matchlist.value = PROXY.matchList
        }
        function setBets(inc, stake) {
            if (!popupData.value.matchData.betAmount){
                popupData.value.matchData.betAmount = 0;
                if (!inc) return
            }
            if (!stake.betAmount || !stake.betsPlaced) {
                stake.betsPlaced = 0;
                stake.betAmount = 0;
                if (!inc) return
            }
            popupData.value.matchData.betAmount = (popupData.value.matchData.betAmount) ? (popupData.value.matchData.betAmount) : 0
            stake.betsPlaced = (stake.betsPlaced) ? (stake.betsPlaced) : 0
            stake.betAmount = (stake.betAmount) ? (stake.betAmount) : 0

            popupData.value.matchData.betAmount += (inc ? 1 : -1) * (stake.price)
            stake.betsPlaced += (inc ? 1 : -1)
            stake.betAmount += (inc ? 1 : -1) * (stake.price)
            
        }

        async function placeBet(e){
            if (!popupData.value.matchData.betAmount){
                showToast("No bets placed");
                return
            }
            try {
                let response = await api.post("/bet/place", popupData.value, getAuthHeaders() )
                console.log('place bet',  response.data)
                // showToast('Your bets has been placed successfully')
            }catch(error){
                console.log(error)
            }
        }
        onMounted(async function () {
            tempProxyMatch()
        })
        return {
            matchlist,
            popupData,

            setBetPopup,
            resetBetPopup,
            setBets,
            placeBet,
        }
    }
}

export default betComponent