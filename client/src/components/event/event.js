import { showToast } from "../../config/toast.js";
import { api } from "../../config/api.js"
import eventTemplate from "./event-template.js"

const { ref } = Vue;
const eventComponent = {
    template: eventTemplate,
    setup(){
        const dateConstraint = `${new Date().getFullYear()}-${(new Date().getMonth()<9)?('0'+(new Date().getMonth()+1)):(new Date().getMonth()+1)}-${new Date().getDate()}`
        const formRef = ref(null)
        const basicEventData = function (){
            return {
                file: "",
                date: "",
                title: "",
                summary: "",
                options: [{
                    _id: 0,
                    name: "",
                }],
                stakes: [{
                    _id: 0,
                    price: "",
                    x: "",
                }],
            }
        }
        let eventData = ref(new basicEventData())

        function addOption (pos){
            eventData.value.options.splice( (pos+1), 0, {_id:"", name: ""} )
        }
        function addStake (pos){
            eventData.value.stakes.splice( (pos+1), 0, {_id:"", price: "", x: ""} )
        }
        async function submitEvent(){
            // eventData.value = new basicEventData();
            let response = await api.post('/event', eventData.value)
            console.log('response', response.data)
            showToast("Event has been added successfully")
        }
        
        return {
            console,
            formRef,
            dateConstraint,
            eventData,
            submitEvent,
            addOption,
            addStake,
        }

    }
}

export default eventComponent