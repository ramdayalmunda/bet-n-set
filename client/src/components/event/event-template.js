const eventTemplate = /*html*/`

<div class="card p-2 m-2">
    <form ref="formRef">
        <div class="card-body">
            <h5 class="card-title mb-3" @click="console.log(eventData)">Add an Event</h5>
            <div class="row col-12 g-4">
                <div class="card">
                    <div class="card-body">

                        <div class="input-group">
                            <input v-model="eventData.file" type="file" class="form-control" id="event-file">
                            <button type="button" class="btn btn-outline-secondary">Upload</button>
                        </div>
                        <div class="progress mt-1" style="height: 2px;">
                            <div class="progress-bar" style="width: 19%"></div>
                        </div>

                        <div class="row">
                            <div class="col-xs-12 col-sm-8">
                                <label class="mt-2">Title*</label>
                                <input v-model="eventData.title" type="text" required class="form-control"
                                    placeholder="Enter Title of the Event">
                            </div>
                            <div class="col-xs-12 col-sm-4">
                                <label class="mt-2">Event Date*</label>
                                <input v-model="eventData.date" type="date" required class="form-control"
                                    placeholder="Event Date YYYY-MM-DD" onfocus="this.showPicker()" :min="dateConstraint"></div>
                        </div>


                        <label class="mt-2">Summary</label>
                        <textarea v-model="eventData.summary" type="text" required class="form-control"
                            placeholder="Enter Summary"></textarea>

                        <label class="mt-2">Options*</label>
                        <div class="row">
                            <template v-for="(option, o) in eventData.options" :key="option">
                                <div class="col-12 mt-1" :name="'option_'+(option._id=o)">
                                    <div class="input-group">
                                        <input v-model="option.name" type="text" class="form-control"
                                            placeholder="Enter Name">
                                        <button class="btn btn-outline-danger" type="button">&Cross;</button>
                                        <button @click="addOption(o)" class="btn btn-outline-success"
                                            type="button">&plus;</button>
                                    </div>
                                </div>
                            </template>
                        </div>

                        <label class="mt-2">Stakes*</label>
                        <div class="row">
                            <template v-for="(stake, s) in eventData.stakes" :key="stake">
                                <div class="col-md-6 col-lg-4 col-xl-4 mt-1" :name="'stake_'+(stake._id=s)">
                                    <div class="input-group">
                                        <span class="input-group-text">&#8377;</span>
                                        <input v-model="stake.price" type="text" class="form-control"
                                            placeholder="Amount">
                                        <input v.model="stake.x" type="text" class="form-control" placeholder="Juice %">
                                        <button class="btn btn-outline-danger" type="button">&Cross;</button>
                                        <button @click="addStake(s)" class="btn btn-outline-success"
                                            type="button">&plus;</button>
                                    </div>
                                </div>
                            </template>
                        </div>


                        <div class="d-flex justify-content-end mt-2">
                            <button type="reset" class="btn btn-secondary">Reset</button>
                            <button @click="submitEvent" type="button" class="btn btn-primary ms-2">Submit</button>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </form>
</div>

`

export default eventTemplate