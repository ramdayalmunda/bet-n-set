export default /*html*/`
<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <div class="container-fluid">
        <span>
            <a class="navbar-brand cursor-pointer" @click="goto('Bet')">Bet-n-Set</a>
            <a class="navbar-brand cursor-pointer" @click="goto('Event')">Add Event</a>
        </span>

        <span>
            <a v-if="!appData.loggedIn" class="navbar-brand cursor-pointer" :class="route.name=='Login'?'disable':''" @click="goto('Login')">Login</a>
            <a v-if="!appData.loggedIn" class="navbar-brand cursor-pointer" :class="route.name=='Register'?'disable':''" @click="goto('Register')">Register</a>
            <a v-if="appData.loggedIn" class="navbar-brand cursor-pointer" @click="logOut">Logout</a>
        </span>
    </div>
</nav>
`