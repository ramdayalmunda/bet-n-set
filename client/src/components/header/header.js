import headTemplate from "./header-template.js";
import router from "../../config/router.js"
import { clearLocalStore } from "../../config/localstore.js";
import { showToast } from "../../config/toast.js";

const { ref } = Vue;
const { useRoute } = VueRouter;
const headComponent = {
    template: headTemplate,
    props:{
        appData: {Object},
    },
    setup(props){
        function goto(name){
            router.push({name: name})
        }
        let route = ref( useRoute() )
        function logOut(){
            props.appData.loggedIn = false;
            clearLocalStore('userData')
            goto('Login')
            showToast("You have logged out successfully!", "success")
        }
        return {
            route,
            
            goto,
            logOut,
        }
    }
}

export default headComponent